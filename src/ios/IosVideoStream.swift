import Foundation

@objc(IosVideoStream) class IosVideoStream : CDVPlugin {
    var videoRenderers : [Int : VideoElementRenderer]!;
    var queue : dispatch_queue_t!
    
    override func pluginInitialize() {
        NSLog("pluginInitialize()");
        self.videoRenderers = [:]
        webView.backgroundColor = UIColor.clearColor();
        webView.opaque = false;
        
        queue = dispatch_queue_create("cordova-plugin-videostream", DISPATCH_QUEUE_SERIAL)
    }
    
    func VideoElementRenderer_new(command: CDVInvokedUrlCommand) {
        NSLog("VideoElementRenderer_new!");
        let id : Int = command.argumentAtIndex(0).integerValue;
        let side : String = command.argumentAtIndex(1) as! String;
        
        let renderer : VideoElementRenderer = VideoElementRenderer(
            webView: self.webView,
            id: id,
            eventListener: { (data: NSDictionary) -> Void in
                let result = CDVPluginResult(status: CDVCommandStatus_OK, messageAsDictionary: data as [NSObject : AnyObject])

                // Allow more callbacks.
                result.setKeepCallbackAsBool(true);
                self.emit(command.callbackId, result: result)
        });
        self.videoRenderers[id] = renderer;
        
        dispatch_async(self.queue) { [weak renderer] in
            renderer?.createSession(side);
        }
    }
    
    func VideoElementRenderer_updateSide(command: CDVInvokedUrlCommand) {
        NSLog("VideoElementRenderer_updateSide!");
        
        let id : Int = command.argumentAtIndex(0).integerValue;
        let side : String = command.argumentAtIndex(1) as! String;
        let renderer = self.videoRenderers[id];
        dispatch_async(self.queue) { [weak renderer] in
            renderer?.setVideoSide(side);
        };
    }
    
    func VideoElementRenderer_takePicture(command: CDVInvokedUrlCommand) {
        NSLog("VideoElementRenderer_refresh!");
        
        let id : Int = command.argumentAtIndex(0).integerValue;
        let renderer = self.videoRenderers[id];
        
        renderer?.takePicture({(data: NSData!) -> Void in
            NSLog("Result, emitting shizzle!");
            self.emit(command.callbackId, result: CDVPluginResult(status: CDVCommandStatus_OK, messageAsArrayBuffer: data))
        });

    }
    
    func VideoElementRenderer_startRecording(command: CDVInvokedUrlCommand) {
        NSLog("VideoElementRenderer_refresh!");
        
        let id : Int = command.argumentAtIndex(0).integerValue;
        let renderer = self.videoRenderers[id];
        
        renderer?.startRecording({(data: NSDictionary!) -> Void in
            NSLog("Result, emitting shizzle!");
            let result = CDVPluginResult(status: CDVCommandStatus_OK, messageAsDictionary: data as [NSObject : AnyObject]);
            result.setKeepCallbackAsBool(true);
            self.emit(command.callbackId, result: result);
        });
    }
    
    func VideoElementRenderer_stopRecording(command: CDVInvokedUrlCommand) {
        let id : Int = command.argumentAtIndex(0).integerValue;
        let renderer = self.videoRenderers[id];
        
        renderer?.stopRecording();
    }
    
    func VideoElementRenderer_refresh(command: CDVInvokedUrlCommand) {
        NSLog("VideoElementRenderer_refresh!");
        let id : Int = command.argumentAtIndex(0).integerValue;
        let data = command.argumentAtIndex(1) as! NSDictionary;
        
        let renderer = self.videoRenderers[id];
        
        renderer?.refresh(data);
    }
    
    func VideoElementRenderer_remove(command: CDVInvokedUrlCommand) {
        NSLog("VideoElementRenderer_remove!");
        let id : Int = command.argumentAtIndex(0).integerValue;
        
        let renderer = self.videoRenderers[id];
        renderer!.remove();
        self.videoRenderers[id] = nil;
    }
    
    
    private func emit(callbackId: String, result: CDVPluginResult) {
        dispatch_async(dispatch_get_main_queue()) {
            self.commandDelegate!.sendPluginResult(result, callbackId: callbackId)
        }
    }
}