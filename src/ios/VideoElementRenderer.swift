//
//  VideoRenderer.swift
//  HelloCordova
//
//  Created by Admin on 9/7/16.
//
//

import Foundation
import AVFoundation

class VideoRecordingDelegate : NSObject, AVCaptureFileOutputRecordingDelegate {
    var callback : (NSDictionary!) -> Void;
    init(callback: (NSDictionary!) -> Void) {
        self.callback = callback;
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        var dict = [String : String]();
        dict["type"] = "startedRecording";
        dict["url"] = fileURL.absoluteString;
        callback(dict);
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!) {
        let urlAsset = AVURLAsset(URL: outputFileURL, options: nil);
        let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetPassthrough);
        
        let outputPath = String.init(format: "%@/Documents/%d.mp4", NSHomeDirectory(), arc4random_uniform(1000000));
        let url = NSURL(fileURLWithPath: outputPath);
        
        exportSession?.outputURL = url;
        exportSession?.outputFileType = AVFileTypeMPEG4;
        exportSession?.shouldOptimizeForNetworkUse = true;
        NSLog("Input size %d", captureOutput.recordedFileSize);
        exportSession?.exportAsynchronouslyWithCompletionHandler{() -> Void in
            //Delete mov
            let fileManager = NSFileManager.defaultManager();
            
            do {
                try fileManager.removeItemAtURL(outputFileURL);
            } catch {
                NSLog("Error removing mov file");
            }
            
            var dict = [String : String]();
            dict["type"] = "recordingFinished";
            dict["url"] = url.absoluteString;
            self.callback(dict);
        };
    }
}

@objc(VideoElementRenderer)
class VideoElementRenderer : NSObject {
    var side : String = "";
    var previewLayer : AVCaptureVideoPreviewLayer?;
    var view : UIView;
    var recordDelegate : VideoRecordingDelegate?;
    var id : Int;
    var webViewController : UIViewController;
    var videoViewController : VideoViewController?;
    var videoView : UIView?;
    var session : AVCaptureSession?;
    var currentVideoInput : AVCaptureDeviceInput?;
    var eventListener : (data: NSDictionary) -> Void;
    var outStillImage : AVCaptureStillImageOutput?;
    var outVideo : AVCaptureMovieFileOutput?;
    
    var savedPositions : NSDictionary?;
    
    init(webView: UIView, id: Int, eventListener: (data: NSDictionary) -> Void) {
        let viewController =  UIApplication.sharedApplication().keyWindow!.rootViewController! as UIViewController;
        
        self.view = viewController.view;
        self.webViewController = viewController;
        self.eventListener = eventListener;
        self.id = id;
        super.init();
        
        self.previewLayer = AVCaptureVideoPreviewLayer();
        self.videoViewController = VideoViewController(previewLayer: previewLayer!);
        self.webViewController.addChildViewController(videoViewController!);
        
        self.videoView = self.videoViewController!.view;
        
        self.videoView!.userInteractionEnabled = false
        self.videoView!.backgroundColor = UIColor.clearColor();
        self.videoView!.hidden = false
        
        
        self.view.addSubview(self.videoView!);
    }
    
    func remove() {
        self.session!.stopRunning();
        self.videoViewController!.removeFromParentViewController();
        self.videoView!.removeFromSuperview();
        self.videoViewController!.remove();
        self.videoView!.hidden = true
    }
    
    func takePicture(callback: (NSData!) -> Void) {
        let connection = outStillImage!.connectionWithMediaType(AVMediaTypeVideo) as AVCaptureConnection;
        outStillImage!.captureStillImageAsynchronouslyFromConnection(connection, completionHandler: {(buffer: CMSampleBuffer!, error: NSError!) -> Void in
            NSLog("Success buffer?");
            if(error != nil) {
                NSLog(error.description);
                return;
            }
            let imageData : NSData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer);
            NSLog("Calling callback!");
            callback(imageData);
        });
    }
    
    func startRecording(callback: (NSDictionary!) -> Void) {
        let outputPath = String.init(format: "%@%d.mov", NSTemporaryDirectory(), arc4random_uniform(1000000));
        let url = NSURL(fileURLWithPath: outputPath);
        recordDelegate = VideoRecordingDelegate(callback: callback);
        outVideo!.startRecordingToOutputFileURL(url, recordingDelegate: recordDelegate);
    }
    
    func stopRecording() {
        outVideo!.stopRecording();
    }
    
    func createSession(side : String) {
        self.session = AVCaptureSession();
        session!.beginConfiguration();
        session!.sessionPreset = AVCaptureSessionPresetMedium;
        session!.commitConfiguration();
        
        let targetAudioDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeAudio);
        
        do {
            let targetAudioDeviceInput = try AVCaptureDeviceInput(device: targetAudioDevice);
            if(session!.canAddInput(targetAudioDeviceInput)) {
                session!.addInput(targetAudioDeviceInput);
            }
            setVideoSide(side);
            
            outStillImage = AVCaptureStillImageOutput();
            if(session!.canAddOutput(outStillImage)) {
                session!.addOutput(outStillImage);
            }
            
            outVideo = AVCaptureMovieFileOutput();
            if(session!.canAddOutput(outVideo)) {
                session!.addOutput(outVideo);
            }
            
            session!.startRunning();
            
            dispatch_async(dispatch_get_main_queue()){
                self.previewLayer?.session = self.session!;
                self.previewLayer?.frame = self.videoView!.bounds;
                self.videoView!.layer.addSublayer(self.previewLayer!);
            };
        } catch {
            NSLog("Fail!");
        }
    }
    
    func setVideoSide(side : String) {
        if(self.side == side) {
            return;
        }
        
        if(currentVideoInput != nil) {
            session!.removeInput(currentVideoInput);
        }
        self.side = side;
        
        var targetDevice : AVCaptureDevice?;
        
        let devices : [AVCaptureDevice] = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo) as! [AVCaptureDevice];
        for device in devices {
            if(side == "front" && device.position == AVCaptureDevicePosition.Front) {
                targetDevice = device;
                break;
            }
            if(side == "back" && device.position == AVCaptureDevicePosition.Back) {
                targetDevice = device;
                break;
            }
            if(side == "other" && device.position == AVCaptureDevicePosition.Unspecified) {
                targetDevice = device;
                break;
            }
        }
        
        do {
            let targetVideoDeviceInput = try AVCaptureDeviceInput(device: targetDevice);
            currentVideoInput = targetVideoDeviceInput;
            
            if(session!.canAddInput(targetVideoDeviceInput)) {
                session!.addInput(targetVideoDeviceInput);
            }
        } catch {
            
        }
    }
    
    func refresh(data: NSDictionary) {
        savedPositions = data;
        let elementLeft = data.objectForKey("elementLeft") as? Float ?? 0
        let elementTop = data.objectForKey("elementTop") as? Float ?? 0
        let elementWidth = data.objectForKey("elementWidth") as? Float ?? 0
        let elementHeight = data.objectForKey("elementHeight") as? Float ?? 0
        let visible = data.objectForKey("visible") as? Bool ?? true
        let opacity = data.objectForKey("opacity") as? Float ?? 1
        let zIndex = data.objectForKey("zIndex") as? Float ?? 0
        let mirrored = data.objectForKey("mirrored") as? Bool ?? false
        let clip = data.objectForKey("clip") as? Bool ?? true
        let borderRadius = data.objectForKey("borderRadius") as? Float ?? 0
        
        NSLog("PluginMediaStreamRenderer#refresh() [elementLeft:%@, elementTop:%@, elementWidth:%@, elementHeight:%@, visible:%@, opacity:%@, zIndex:%@, mirrored:%@, clip:%@, borderRadius:%@]",
              String(elementLeft), String(elementTop), String(elementWidth), String(elementHeight),
              /*String(videoViewWidth), String(videoViewHeight),*/ String(visible), String(opacity), String(zIndex),
                                                                   String(mirrored), String(clip), String(borderRadius))
        
        self.videoView!.frame = CGRectMake(
            CGFloat(elementLeft),
            CGFloat(elementTop),
            CGFloat(elementWidth),
            CGFloat(elementHeight)
        )
        
        if visible {
            self.videoView!.hidden = false
        } else {
            self.videoView!.hidden = true
        }
        
        self.videoView!.alpha = CGFloat(opacity)
        self.videoView!.layer.zPosition = CGFloat(zIndex)
        
        // if the zIndex is 0 (the default) bring the view to the top, last one wins
        if zIndex == 0 {
            self.view.superview?.bringSubviewToFront(self.videoView!)
        }
        
        if !mirrored {
            self.videoView!.transform = CGAffineTransformIdentity
        } else {
            self.videoView!.transform = CGAffineTransformMakeScale(-1.0, 1.0)
        }
        
        if clip {
            self.videoView!.clipsToBounds = true
        } else {
            self.videoView!.clipsToBounds = false
        }
        
        self.videoView!.layer.cornerRadius = CGFloat(borderRadius);
    }
    
}