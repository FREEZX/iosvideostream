//
//  VideoView.swift
//  HelloCordova
//
//  Created by Admin on 9/7/16.
//
//

import Foundation
import AVFoundation
import UIKit

class VideoViewController : UIViewController {
    var previewLayer : AVCaptureVideoPreviewLayer?;
    var veRenderer : VideoElementRenderer?;
    
    init(previewLayer : AVCaptureVideoPreviewLayer) {
        self.previewLayer = previewLayer;
        super.init(nibName: nil, bundle: nil);
        self.view.layer.addSublayer(previewLayer);
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    }
    
    required init?(coder aDecoder: NSCoder) {
        previewLayer = nil;
        super.init(coder: aDecoder);
    }
    
    func remove() {
        previewLayer!.removeFromSuperlayer();
    }
    
    override func shouldAutorotate() -> Bool {
        return true;
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return .Portrait;
    }
}