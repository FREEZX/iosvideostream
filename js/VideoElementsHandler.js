var exec = require('cordova/exec');

var renderers = {};

module.exports.renderers = renderers;

module.exports.takePhoto = function(id, callback) {
	exec(callback, null, 'IosVideoStream', 'VideoElementRenderer_takePicture', [id]);
};

module.exports.startRecording = function(id, callback) {
	exec(callback, null, 'IosVideoStream', 'VideoElementRenderer_startRecording', [id]);
};

module.exports.stopRecording = function(id, callback) {
	exec(null, null, 'IosVideoStream', 'VideoElementRenderer_stopRecording', [id]);
};

var VideoElementRenderer = require('./VideoElementRenderer');
var Random = require('random-js');

var generateRandom = Random.integer(1, 1000000);
var engine = Random.engines.mt19937().autoSeed();

var videoObserver = new MutationObserver(function(mutations) {
  console.log('Mutation!');
  for(var i=0; i<mutations.length; ++i) {
    if(mutations[i].type == 'childList') {
      console.log(mutations[i]);
      //Added nodes
      var j;
      var video;
      for(j=0; j<mutations[i].addedNodes.length; ++j) {
        console.log(mutations[i].addedNodes);
        if(mutations[i].addedNodes[j].getElementsByTagName) {
          video = mutations[i].addedNodes[j].getElementsByTagName("IOS-VIDEO");
          if(video.length && !video[0].getAttribute('renderer-id')) {
            //Attach the renderer on this node
            attachVideo(video[0]);
          }
        }
      }
      //Removed nodes
      for(j=0; j<mutations[i].removedNodes.length; ++j) {
        if(mutations[i].removedNodes[j].getElementsByTagName) {
          video = mutations[i].removedNodes[j].getElementsByTagName("IOS-VIDEO");
          console.log(video);
          if(video.length) {
            //Remove the renderer on this node
            removeVideo(video[0]);
          }
        }
      }
    }
  }
});

var config = {
	// Set to true if additions and removals of the target node's child elements (including text nodes) are to
	// be observed.
	childList: true,
	// Set to true if mutations to not just target, but also target's descendants are to be observed.
	subtree: true
};

function attachVideo(node) {
	var side = node.getAttribute('side') || 'front';

	//Ensure id is unique
	var alreadyExists;
	var id;
	do {
		id = generateRandom(engine);
		alreadyExists = renderers.hasOwnProperty(id);
	} while (alreadyExists === true);

	var renderer = new VideoElementRenderer(node, id, side);
	renderers[renderer._id] = renderer;
}

function removeVideo(node) {
	var id = node.getAttribute('renderer-id');
	var videoElement = renderers[id];
	videoElement.remove();
	delete renderers[id];
}

//Detect new objects
videoObserver.observe(document.body, config);

//Process the old ones
var tags = document.getElementsByTagName("IOS-VIDEO");
for(var i=0; i<tags.length; ++i) {
	attachVideo(tags[i]);
}

function _refreshAll() {
	for(var key in renderers) {
		renderers[key].refresh();
	}
}

window.addEventListener('resize', function() {
	_refreshAll();
}, true);
